//
// File: Problem.java
// Author: Daniel Devine
// Purpose: Defines the interface for a Problem object. 
//

public interface Problem {
	// This interface defines a basic Problem object. When making a new type of Problem you must implement this.

	String toString(); // Returns the question.
	String getHint(); // Returns a hint. 
	boolean checkAnswer(String ans); // Checks the given answer. Returns result.
	boolean isSolved(); // Returns if the question has been solved.
	void setStartNode(boolean b); // Sets the Start node variable
	boolean isStartNode(); // Returns true if the Problem is a start node.
	void setFinishNode(boolean b); // Sets the Finish node variable
	boolean isFinishNode(); // Returns true if the Problem is a Finish node.
	String getID(); // Returms the node ID
}