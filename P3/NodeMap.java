//
// File: NodeMap.java
// Author: Daniel Devine
// Purpose: A class which defines a "canvas" which grapically represents the map.
//

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class NodeMap extends JPanel {

	// These width and height variables are currently unused.
	private int framewidth = MinimusHacker.mapwidth; // Set the animation area width to be the mapWidth defined in the main class.
	private int frameheight = MinimusHacker.mapheight; // Set the animation area height to be the mapHeight defined in the main class.

	private Problem[][] nodelayout; // The 2D node grid

	private float alphalevel = 1f; // Tracks the current level of the alpha channel for fading animations.
	public final float ALPHA_MIN = 0f;  // Defines the minimum level of the alpha channel. 0 results in opaque.
	public final float ALPHA_MAX = 200f; // Defines the maximum level of the alpha channel. Much higher than 200 and things turn pink...
	private int direction = 0; // Direction of the fading. 1 for in, 0 for out.
	public final float ALPHA_INC = 4.2f; // The amount to increment the alpha channel fading by.

	public NodeMap(Problem[][] nodelayout){
		this.nodelayout = nodelayout;
		this.setOpaque(true);
	}

	private int getCurrentAlpha(){
		// Return the level of the alpha channel and increment/decrement in a fade-in/fade-out pattern.
		
		//System.out.println("alpha: " + alphalevel + " direction: " + direction);
		if (direction == 0 && alphalevel < ALPHA_MAX){ // fade out (increase alpha)
			alphalevel = (alphalevel + ALPHA_INC < ALPHA_MAX) ? alphalevel + ALPHA_INC : ALPHA_MAX;
			return (int) alphalevel;
		} else {
			direction = 1;
		}

		if (direction == 1 && alphalevel > ALPHA_MIN){ // fade in (decrease alpha)
			alphalevel = (alphalevel - ALPHA_INC > ALPHA_MIN) ? alphalevel - ALPHA_INC : ALPHA_MIN;
			return (int) alphalevel;
		} else {
			direction = 0;
		}
		return (int) ALPHA_MIN;
	}
	
	public void paintComponent(Graphics g) {
		// This is for painting the node grid.
		super.paintComponent(g); // Force the container above this to paint, avoids graphic corruption.
 		Graphics2D g2d = (Graphics2D) g.create();

 		// Jack up some rendering settings for the lulz (mostly).
 		RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
 		qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY); 
		qualityHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);	// Text antialiasing... Might help.
		g2d.setRenderingHints(qualityHints); 

	
		Color alpha = new Color(255, 255, 255, getCurrentAlpha()); // Set the current alpha channel we are going to overlay.
		for (int i = 0; i < 5; i++){
 			for (int j = 0; j < 5; j++){
 				if (nodelayout[i][j] != null){
 					Problem node = nodelayout[i][j];
 					if (node.isSolved()){			// Solved nodes are Green
						g2d.setPaint(Color.GREEN);
						g2d.fillRect(99 * i, 99 * j, 99, 99);
 						g2d.setPaint(alpha);
 						g2d.fillRect(99 * i, 99 * j, 99, 99);
 						g2d.setPaint(Color.BLACK);
 						g2d.drawRect(99 * i, 99 * j, 99, 99);
 						g2d.drawString(node.getID(), (99 * i) + 40, (99 * j) + 60);
 						if (node.isStartNode()){	// If it is the Start node, put some text.
 							g2d.drawString("Start", (99 * i) + 50, (99 * j) + 30);
 						}
 					} else if(node.isFinishNode()){ // The Finish node is Orange.
 						g2d.setPaint(Color.ORANGE);
						g2d.fillRect(99 * i, 99 * j, 99, 99);
 						g2d.setPaint(alpha);
 						g2d.fillRect(99 * i, 99 * j, 99, 99);
 						g2d.setPaint(Color.BLACK);
 						g2d.drawRect(99 * i, 99 * j, 99, 99);
 						g2d.drawString(node.getID(), (99 * i) + 40, (99 * j) + 60);
 						g2d.drawString("Finish", (99 * i) + 50, (99 * j) + 30);
 					} else {						// Uncracked nodes are Red and don't fade.
 						g2d.setPaint(Color.RED);
						g2d.fillRect(99 * i, 99 * j, 99, 99);
 						g2d.setPaint(Color.BLACK);
 						g2d.drawRect(99 * i, 99 * j, 99, 99);
 						g2d.drawString(node.getID(), (99 * i) + 40, (99 * j) + 60);
 					}
 				}
 			}

 		}
	}

}
