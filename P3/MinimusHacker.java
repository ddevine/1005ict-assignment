//
// File: MinimusHacker.java
// Author: Daniel Devine
// Purpose: The main class. This defines the UI and the game logic. 
//

import java.awt.*;
import java.net.URI;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.text.*;
import javax.swing.event.*;

class MinimusHacker {
	private static JFrame frame = new JFrame("Minimus Hacker"); // The main Window for the program.
	private static JTextArea stdout = new JTextArea(10, 50); // The output text area
	private static JTextField stdin = new JTextField(50); // The input text field
	private static JScrollPane stdoutscroll; // The scrollpane for the output text area.

	public final static int mapwidth = 500; // The width of the node map.
	public final static int mapheight = 500; // The height of the node map.

	private static NodeMap map; // A reference to our Node Map "canvas"

	private static Problem[][] nodelayout; // The node grid

	private static boolean exitSure = false; // Tracks whether the user is sure they want to use the exit command.

	private static int currenti = 0; // The current "x" axis grid reference the user is operating on.
	private static int currentj = 0; // The current "y" axis grid reference the user is operating on.

	private static boolean probed = false; // Tracks if the current node has been probed yet.

	private static long starttime = -1; // Tracks when the user started hacking. Unix time in milliseconds.
	private static long finishtime = -1; // Tracks when the user finshed hacking. Unix time in milliseconds.

	private static LinkedList<String> triedList = new LinkedList<String>(); // Tracks the list of attempted nodes in a path resolution.
	private static LinkedList<String> attemptPath = new LinkedList<String>(); // Tracks the attempted path in a path resolution.

	private static String startNodeID = "A0"; // The startNodeID, defaults to top left.

	public final static int PENALTY_UNIT = 2000; // The amount of time in milliseconds that a user is penalised by.

	private static boolean getWinner = false; // Tracks if the username is expected to be entered.

	public static void main(String[] args){
		setup_map();
		setup_layout();
		addListeners();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);

		Animator a = new Animator(map);
		a.animate(30);
	}

	private static void setup_map(){
		boolean foundmap = false;	
		while(!foundmap){ // While the map is un-winnable, make a new map. Cool code bro.
			nodelayout = makeNodeLayout();
			try {
				if (canFindFinish(startNodeID, 0)){
					foundmap = true;
					triedList.removeAll(triedList); // Clean up
					attemptPath.removeAll(attemptPath); // Clean up, though this shouldn't be needed.
				}
			} catch (NullPointerException e){
				System.err.println("Null Pointer error in canFindFinish again..."); // Don't have time to properly fix this.
			}
		}
		map = new NodeMap(nodelayout);
	}
	

	private static void setup_layout(){
		// Initialises the layout of the GUI.

		JPanel box0 = new JPanel(); // Outer wrapper
		JPanel box1 = new JPanel(); // Animation area...
		JPanel box2 = new JPanel(); // stdin/stdout area.

		box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS));
		box1.setLayout(new BoxLayout(box1, BoxLayout.Y_AXIS));
		box2.setLayout(new BoxLayout(box2, BoxLayout.Y_AXIS));

		frame.add(box0);
		box0.add(box1);
		box0.add(box2);

		box0.setAlignmentX(Component.CENTER_ALIGNMENT);
		box1.setAlignmentX(Component.CENTER_ALIGNMENT);
		box2.setAlignmentX(Component.CENTER_ALIGNMENT);

		box0.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
		box2.setBorder(new EmptyBorder(5, 5, 5, 5));

		map.setPreferredSize(new Dimension(mapwidth, mapheight));

		box1.add(map);
		stdoutscroll = new JScrollPane(stdout);
		box2.add(stdoutscroll);
		box2.add(stdin);

		stdin.setFocusable(true);
		stdin.requestFocus();
		stdout.setEditable(false);

		stdin.setText("Type commands here and press ENTER. Type \"help\" for command list.");
		stdout.setText("Use the \"target\" command to target the next node.\n");
	}

	private static void addListeners(){
		// Adds Listeners to the GUI. These listen for Mouse, Keyboard and Document (stdout) events.
		stdin.addKeyListener(
			new KeyAdapter(){ // Unlike ActionListener in Program 1, KeyListener isn't abstract. This works around that.
				public void keyReleased(KeyEvent e){
					if(e.getKeyCode() == KeyEvent.VK_ENTER){
						String input = cleanInput(stdin.getText());
						if (input.compareToIgnoreCase("") != 0){
							parseInput(input);
							resetPrompt();
						}
					}
    			}
			}
		); // end stdin listener

		stdin.addMouseListener(
			new MouseAdapter(){ // Same deal as KeyAdapter...
				public void mouseClicked(MouseEvent e) {
					resetPrompt();
				}
			}
		);

		// stdout auto-scrolling.
		stdout.getDocument().addDocumentListener( // We need to listen for when text is added to stdout. There is no adapter for DocumentListener.
			new DocumentListener(){
				public void insertUpdate(DocumentEvent e) {
					JScrollBar bar = stdoutscroll.getVerticalScrollBar();
					bar.setValue(bar.getMaximum());
					System.out.println("Scrolling..."); // RETARDED HACK: If I don't have this println here scrolling stops working (WTF! I don't even know man...)
		        }
		        public void removeUpdate(DocumentEvent e) {}
		        public void changedUpdate(DocumentEvent e) {}
		    }
		);

	} // End addListeners

	private static String cleanInput(String text){
		// Remove "#" at the front and trim both ends of the String.
		return text.replaceFirst("^#", "").trim();
	}

	private static void parseInput(String cmdline){
		// This parses user commands and fires the appropriate methods.

		stdout.append("> " + cmdline + "\n");
		Scanner sc = new Scanner(cmdline);
		String cmd = sc.next();

		if (starttime == -1){
			starttime = System.currentTimeMillis();
		}

		if (cmd.compareToIgnoreCase("exit") == 0){
			cmdExit();
			return;
		} else {
			if (exitSure){
				exitSure = false; // Reset this back, because we're not continuing with exit.
			}
		}

		// I don't want to assume the user has JRE 7 for a switch statement with strings...
		if (cmd.compareToIgnoreCase("probe") == 0){
			cmdProbe();
			return;
		}

		if (cmd.compareToIgnoreCase("target") == 0){
			cmdTarget(sc.next());
			return;
		}

		if (cmd.compareToIgnoreCase("help") == 0){
			cmdShowHelp();
			return;
		}

		if (cmd.compareToIgnoreCase("hint") == 0){
			cmdShowHint();
			return;
		}

		if (getWinner == true){
			cmdSubmitScore(cmdline);
			return;
		}

		// Catch answers.
		if (probed == true){
			cmdAnswer(cmdline);
		} else {
			stdout.append("Invalid Command\n");
		}
	}

	private static void cmdSubmitScore(String name){
		// This submits a score onto the scoreboard when a user finishes. Opens a browser window.
		String urlstring = "http://stuff.ddevnet.net/minimus/minimus.php?name="+name+"&score=" + (finishtime - starttime)/1000;
	        try {
	        	Desktop desktop = Desktop.getDesktop();
               	URI uri = new URI(urlstring);
                desktop.browse(uri);
            }
            catch ( Exception e ) {
                stdout.append(e.getMessage() + "\n");
            }	
	}

	private static void cmdShowHint(){
		// Show a hint for the current node. Give user a penalty for doing so.
		stdout.append(nodelayout[currenti][currentj].getHint() + "\n");
		starttime -= (2 * PENALTY_UNIT);
	}

	private static void cmdShowHelp(){
		// The command to show help text in the output window.
		String helptext = "\"help\"\t - Shows this help text.\n" +
							"\"target NODE\" - Targets the specified NODE (ie. A0, B3, D4).\n" +
							"\"probe\"\t - Probes the targeted node to reveal an exploit to work on.\n" +
							"\"hint\"\t - Provides a hint on the current exploit.\n" +
							"\"exit\"\t - exit the MinimusHack.\n\n" +

							"Instructions:\n" +
							"At the start node (in green) target a node adjacent (but not diagonal)\n" +
							"to the start node. Probe the node to detect an explot and then enter an answer.\n" +
							"answers result in a small time penalty. If you need more help finding an answer you\n" +
							"can use the hint command but this will incur a large time penalty. Correct answers\n" +
							"give a small time bonus. \n" + 
							"Your goal is to make it to the finish node (in orange) as quickly as possible.\n";
		stdout.append(helptext);
	}

	private static void cmdTarget(String id){
		// The command to target a node on the grid. An id is a grid reference like A0, A1, B4...
		id = id.toUpperCase();
		int[] idxs = getNodeIndexByID(id);
		String failmessage = "You cannot target " + id + " because it is not a valid ID or not targetable.\n";


		try {
			if (!nodelayout[idxs[0]][idxs[1]].isSolved() || isSolvedInRange(id)){ // If the requested node isn't solved and there's a solved node in range.
				if (nodelayout[idxs[0]][idxs[1]] != null){
					Problem current = nodelayout[currenti][currentj];
					boolean acceptnew = false;
					// Check north, south, east, west to see if next node is in range.
					if (isValidMove(current.getID(), id)){
						acceptnew = true;
					} else {
						stdout.append(failmessage);
					}

					if (acceptnew){
						currenti = idxs[0];
						currentj = idxs[1];
						stdout.append("Targed node " + id + "\n");
						probed = false;
					}
					
				} else {
					stdout.append(failmessage);
				}
			} else {
					currenti = idxs[0];
					currentj = idxs[1];
					stdout.append("Targed node " + id + "\n");
					probed = false;
			}
		} catch (Exception e) { // Probably index out of bounds.
			stdout.append(failmessage);
		}
	}

	private static boolean isValidMove(String from, String to){
		// Checks if it is valid to move between two nodes given node IDs.

		int[] fromindex = getNodeIndexByID(from);
		int[] toindex = getNodeIndexByID(to);

		int fromi = fromindex[0];
		int fromj = fromindex[1];

		int toi = toindex[0];
		int toj = toindex[1];

		// Check north, south, east, west to see if next node is in range.
		if (fromi == toi && fromj == toj + 1){ // north
			try{ // Check if the node we are going to is valid.
				if (nodelayout[toi][toj] != null){
					return true;
				} else {
					return false;
				}
			} catch(Exception e){ // Probably index out of bounds
				return false;
			}
		}

		if(fromi == toi - 1 && fromj == toj){ // east
			try{ // Check if the node we are going to is valid.
				if (nodelayout[toi][toj] != null){
					return true;
				} else {
					return false;
				}
			} catch(Exception e){ // Probably index out of bounds
				return false;
			}
		}

		if(fromi == toi && fromj == toj - 1){ // south
			try{ // Check if the node we are going to is valid.
				if (nodelayout[toi][toj] != null){
					return true;
				} else {
					return false;
				}
			} catch(Exception e){ // Probably index out of bounds
				return false;
			}
		}

		if(fromi == toi + 1 && fromj == toj){ // west
			try{ // Check if the node we are going to is valid.
				if (nodelayout[toi][toj] != null){
					return true;
				} else {
					return false;
				}
			} catch(Exception e){ // Probably index out of bounds
				return false;
			}
		}

		return false;
	}

	private static boolean isSolvedInRange(String id){
		// Tries to find if there is a solved node within range of the given node ID.

	 	int[] idxs = getNodeIndexByID(id);

	 	int i;
	 	int j;

	 	try {
	 		i = idxs[0];
	 		j = idxs[1] - 1;
	 		if (nodelayout[i][j].isSolved()){ // north
				return true;
			}
		} catch (Exception e){}

		try {
	 		i = idxs[0] + 1;
	 		j = idxs[1];
	 		if (nodelayout[i][j].isSolved()){ // east
				return true;
			}
		} catch (Exception e){}

	 	try {
	 		i = idxs[0];
	 		j = idxs[1] + 1;
	 		if (nodelayout[i][j].isSolved()){ // south
				return true;
			}
		} catch (Exception e){}

	 	try {
	 		i = idxs[0] - 1;
	 		j = idxs[1];
	 		if (nodelayout[i][j].isSolved()){ // west
				return true;
			}
		} catch (Exception e){}

		return false;

	 }

	private static boolean canFindFinish(String node, int level){
		// Attempts to find the Finish node from this node. Returns true if the finish can be navigated to.

		// RECURSIVE FUNCTION DESIGN NOTES - This function treats the grid as a tree, doing a depth first search.

		// else if level = -1;
			// return false, because we have bottomed out.

		// if head is finish.
			// return true
		// else add self to tried.

		// if list is only self or all child nodes tried.
			// remove self from attemptpath
			// return (recurse with node at level - 1, level -1)
		// else if there are still nodes to try.
			// add self to attempt path.
			// return (recurse with untried child, level + 1)

		LinkedList<Problem> nodesinrange = getNodesInRange(node);

		if (level == -1){
			return false; // All done.
		}

		if (nodesinrange.get(0).isFinishNode()){
			return true;
		} else {
			triedList.add(node);
		}

		if (childrenAllTried(nodesinrange)){ // Think about this!
			//System.out.println("ALL NODES TRIED");
			attemptPath.remove(node);
			if (level != 0){
				return canFindFinish(attemptPath.get((level-1)), level - 1);
			} else {
				return false; // I hope so...
			}
		} else {
			attemptPath.add(node);
			return canFindFinish(untriedChild(nodesinrange), level + 1);
		}
	}

	private static String untriedChild(LinkedList<Problem> nodes){
		// Check the given list of node IDs and return an ID if it has not been tried in the path resolution.

		for (Problem p: nodes){
			if (p != null && !triedList.contains(p.getID())){ //If it is not null and not in the tried list, return it.
				return p.getID();
			}
		}
		return null;
	}

	private static boolean childrenAllTried(LinkedList<Problem> list){
		// Return true if the nodes in the given list have been tried in a path resolution.

		for (Problem p: list){
			if (p != null && !triedList.contains(p.getID())){ //If it is not null and not in the tried list, then we have an untried item!
				return false;
			}
		}
		return true;
	}

	private static LinkedList<Problem> getNodesInRange(String node){
		// Returns a list of nodes in range, including self, given the node ID. This will most often return 2 nodes (self and previous node.)

		LinkedList<Problem> list = new LinkedList<Problem>();

		if (node == null){
			System.err.println("NULL NODE STRING IN getNodesInRange");
		} 

		list.add(getNodeByID(node));

		//System.out.println("Self should be: " + node);
		//System.out.println("Adding self: " + list.get(0).getID());

		int[] idxs = getNodeIndexByID(node);

		int i;
	 	int j;

		try {
	 		i = idxs[0] + 1;
	 		j = idxs[1];
	 		if (nodelayout[i][j] != null){ // east (first because it is most optimal)
				list.add(nodelayout[i][j]);
			}
		} catch (Exception e){}

	 	try {
	 		i = idxs[0];
	 		j = idxs[1] + 1;
	 		if (nodelayout[i][j] != null){ // south
				list.add(nodelayout[i][j]);
			}
		} catch (Exception e){}

	 	try {
	 		i = idxs[0] - 1;
	 		j = idxs[1];
	 		if (nodelayout[i][j] != null){ // west
				list.add(nodelayout[i][j]);
			}
		} catch (Exception e){}

		try {
	 		i = idxs[0];
	 		j = idxs[1] - 1;
	 		if (nodelayout[i][j] != null){ // north
				list.add(nodelayout[i][j]);
			}
		} catch (Exception e){}

		return list;
	}


	private static void cmdAnswer(String cmdline){
		// Checks the user's answer against the Problem object and gives appropriate rewards.

		if (!nodelayout[currenti][currentj].checkAnswer(cmdline)){
			stdout.append("Attempt failed. Time penalty incurred.\n");
			starttime -= PENALTY_UNIT;	// Add time penalty.
		} else {
			stdout.append("Attempt successful. Time bonus.\n");
			starttime += PENALTY_UNIT;
			probed = false;
			if (nodelayout[currenti][currentj].isFinishNode()){
				gameFinished();
			}

		}

	}

	private static void cmdProbe(){
		// Returns the problem string for a node.

		if (!nodelayout[currenti][currentj].isSolved()){
			probed = true;
			stdout.append("Probing...\n"); // We should find a nice way to pause here...
			stdout.append(nodelayout[currenti][currentj].toString() + ":\n");
		} else {
			stdout.append("This node is already cracked.\n");
		}
	}

	private static void resetPrompt(){
		// Resets the input prompt to default.
		stdin.setText("# ");
	}

	private static void cmdExit(){
		// Exits the game if the user has confirmed to do so.

		if(exitSure){
			System.exit(0);
		} else {
			exitSure = true;
			stdout.append("Are you sure you want to exit? Type exit again if you are.\n");
			resetPrompt();
		}
	}

	private static Problem[][] makeNodeLayout(){
		// Generates the node grid with pseudo-random Problems, then sets start/finish nodes and returns the generated map.

		int n = 5;
		Problem[][] map = new Problem[n][n];

		for(int i = 0; i < n; i++){
			for (int j = 0; j < n; j++){
				int r =  new Random().nextInt(3);
				if (r == 0){
					map[i][j] = new NumberGuess(encodeNodeID(i, j));
				} else if (r == 1){
					map[i][j] = new NumberGuess(encodeNodeID(i, j)); // I'll make a new problem type if I get time.
				} else if (r == 2){
					map[i][j] = null;
				}
			}
		}

		return setStartFinish(map, 0, 0, n);
	}

	private static Problem[][] setStartFinish(Problem[][] map, int i, int j, int n){
		// Sets the start node at the top-left-most node, then sets Finish at bottom-right-most node and returns the map.

		// i and j are the point to start looking from. If that point doesn't work then increment j and eventually i.

		if (map[i][j] != null){
			// Now check that it is not a (completely) isolated node.
			if (map[i][j+1] != null || map[i+1][j] != null){ 
				map[i][j].setStartNode(true);
				startNodeID = map[i][j].getID();
				//System.out.println("Startnode at: " + i + ":" + j);
				currenti = i;
				currentj = j;
				// Go and find the finish now...
				map = setFinish(map, n-1, n-1, n); // Go off and set our finish node.
				return map;
			}
		}

		// Check the next left-top most place.
		if (j >= (n - 2)){
			i++;
			j = 0;
		} else {
			j++;
		}

		//System.out.println("i: " + i + " j: " + j + " n: " + n);
		return setStartFinish(map, i, j, n); // Recurse
	}

	private static Problem[][] setFinish(Problem[][] map, int i, int j, int n){
		// Returns a map with the Finish node at the bottom-left-most node available.

		// i and j are the point to start looking from. If that point doesn't work then decrement j and eventually i.
		if (map[i][j] != null){
			// Now check that it is not a (completely) isolated node.
			if (map[i][j-1] != null || map[i-1][j] != null){
				map[i][j].setFinishNode(true);
				//System.out.println("Finishnode at: " + i + ":" + j);
				return map;
			}
		}

		// Check the next left-top most place.
		if (j == 1){
			i--;
			j = n - 1;
		} else {
			j--;
		}

		//System.out.println("i: " + i + " j: " + j);
		return setFinish(map, i, j, n); // Recurse
	}

	private static String encodeNodeID(int i, int j){
		// Encodes the given grid coordinates into a node ID.
		return ""+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(i) + j;
	}

	private static Problem getNodeByID(String id){
		// Returns the problem at the given node ID.
		if(id == null){
			System.err.println("NULL STRING given to getNodeByID!!!");
		}
		id = id.toUpperCase();
		//System.out.println("NodeByID: " + id);
		int i = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(id.charAt(0));
		return nodelayout[i][Integer.parseInt(""+id.charAt(1))]; // Index out of range lol.
	}

	private static int[] getNodeIndexByID(String id){
		// Decodes a node ID string and returns i and j in a 2 element integer array.
		try{
			int[] ans = new int[2];
			id = id.toUpperCase();
			ans[0] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(id.charAt(0));
			ans[1] = Integer.parseInt(""+id.charAt(1));
			nodelayout[ans[0]][ans[1]].getID();
			return ans;
		} catch (Exception e) {
			return null;
		}

	}

	private static void gameFinished(){
		// Do stuff for the finished game...

		finishtime = System.currentTimeMillis();
		stdout.append("Hack finished in " + (finishtime - starttime)/1000 + " seconds! Enter your name for the scoreboard: \n");
		getWinner = true;
	}

}
