//
// File: Animator.java
// Description: An animator class that automatically fires off repaint, heavily based on the 1005ICT version by Andrew Rock.
// 		Origional at: http://www.ict.griffith.edu.au/arock/oop-12-2/students/workshop/13/Nathan/Animator.java
//

import javax.swing.*;
import java.util.*;

public class Animator extends TimerTask {
	// An Animator class, to fire paintComponent of the given JPanel periodically.

	private JPanel canvas;
	private java.util.Timer timer = new java.util.Timer(); 

	public Animator(JPanel canvas){
		this.canvas = canvas;
	}

	public void animate(int fps){
		timer.scheduleAtFixedRate(this, 0, 1000L / fps);
	}

	public void run(){
		canvas.repaint();
	}
}
