//
// File: NumberGuess.java
// Author: Daniel Devine
// Purpose: A class that implements the Problem interface. 
//			This type of problem makes the user guess a number in order to crack the node.
//

import java.util.*;

class NumberGuess implements Problem {
	// A number guessing type of problem.

	private boolean solved = false; // Tracks if the Problem has been solved or not.
	private boolean startnode = false; // Tracks if this node is the Start node.
	private boolean finishnode = false; // Track if this node is the Finish node.

	private int x; // The secret number.

	private String id; // The ID of the node. This is a grid reference.

	private int upperlim; // The upper limit of the range the secret number is in.
	private int lowerlim; // The lower limit of the range the secret number is in.
	private int lastanswer; // Tracks the last answer the user tried, used for hinting.

	public NumberGuess(String id){
		// set up the random parts of the question here.
		// Set the secret number, then random upper and lower limits...

		this.id = id;
		int max = 100;

		int r = new Random().nextInt(max);
		while(r == 0 || r < (max / 5)){ // We want a number in the middle-ish, so not too low or too high in relation to max.
			r = new Random().nextInt(max);
		}
		this.x = r;

		this.upperlim = this.x + (this.x / 2) + new Random().nextInt(15);
		this.lowerlim = this.x - (this.x / 2) - new Random().nextInt(15);

		if (new Random().nextInt(10) > 4){
			this.lastanswer = (this.x / 2) + new Random().nextInt(15);
		} else {
			this.lastanswer = (this.x / 2) - new Random().nextInt(15);
		}
	}

	public String toString(){
		// Returns the problem to solve.
		return "Find number between " + lowerlim  + " and " + upperlim;
	}

	public String getHint(){
		// Gives higher or lower based on the last answer given (or default).
		String dir = (x > lastanswer) ? "greater" : "less";
		return "The number is " + dir + " than " + lastanswer + " (penalty incurred)";
	}

	public boolean checkAnswer(String ans){
		// Checks the answer given against the secret number, if matches returns true.
		lastanswer = Integer.parseInt(ans);
		boolean res = (x == lastanswer) ? true : false;
		//System.out.println("x: " + x + " try: " + lastanswer + " result: " + res);
		solved = res;
		return res;
	}

	public boolean isSolved(){
		// Returns true if the problem is solved.
		return solved;
	}

	public void setStartNode(boolean b){
		// Sets this node to be the Start node. The start node is automatically solved.
		this.solved = b;
		this.startnode = b;
	}

	public boolean isStartNode(){
		// Returns true if this problem is the Start node.
		return this.startnode;
	}

	public void setFinishNode(boolean b){
		// Sets if this node is the Finish node.
		this.finishnode = b;
	}

	public boolean isFinishNode(){
		// Returns true if this Problem is the Finish node.
		return this.finishnode;
	}

	public String getID(){
		// Returns the grid reference (ID) of this Problem.
		return id;
	}
}