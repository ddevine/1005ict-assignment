//
// File: StorageManager.java
// Author: Daniel Devine
// Purpose: This defines a Storage Manager. The job of a Storage Manager is to act as an abstraction 
// 			for the ticket storage operations.
//

import java.io.*;
import java.util.*;

public class StorageManager {
	// The class for a Storage Manager, which manages saving and loading of Tickets.

	private static String openTicketsPath; // The path/filename for the open tickets database.
	private static String closedTicketsPath; // The path/filename for the closed tickets database.

	private static LinkedList<Ticket> ticketQueue; // Queue for open tickets
	private static LinkedList<Ticket> closedTicketList; // List of closed tickets

	public StorageManager(String openTicketsPath, String closedTicketsPath, LinkedList<Ticket> ticketQueue, LinkedList<Ticket> closedTicketList){
		this.openTicketsPath = openTicketsPath;
		this.closedTicketsPath = closedTicketsPath;

		this.ticketQueue = ticketQueue;
		this.closedTicketList = closedTicketList;
	}	
	public void save(){
		// Save open and closed tickets to respective files.

		// Open Tickets file
		try {
				PrintStream out = new PrintStream(openTicketsPath);
				for (Ticket t:ticketQueue){
					out.println(t.getID());
					out.println(t.getPhoneNumber());
					out.println(t.getDescLineCount());
					out.println(t.getDescription());
					out.println(t.getOpenDate());
				}
				out.flush();
				out.close();
			} catch (Exception e) {
				System.err.println("Failed to save " + openTicketsPath);
				System.err.println(e);
			}

		// Closed Tickets file
		try {
				PrintStream out = new PrintStream(closedTicketsPath);
				for (Ticket t:closedTicketList){
					out.println(t.getID());
					out.println(t.getPhoneNumber());
					out.println(t.getDescLineCount());
					out.println(t.getDescription());
					out.println(t.getOpenDate());
					out.println(t.getCloseDate());
				}
				out.flush();
				out.close();
			} catch (Exception e) {
				System.err.println("Failed to save " + closedTicketsPath);
				System.err.println(e);
			}
		return;
	}



	public void load() {
		// Load open and closed tickets from respective files.

		long id;
		String phonenum;
		int lines;
		String desc;
		long opendate;
		long closedate;

		try {
		Scanner scOT = new Scanner(new File(this.openTicketsPath));

			while (scOT.hasNext()){
				id = scOT.nextLong();
				phonenum = scOT.next();
				lines = scOT.nextInt();
				desc = "";
				scOT.nextLine(); // The scanner was between the int and the newline, so we advance it...

				for (int l = 0; l < lines; l++) {
					desc += scOT.nextLine();
				}

				opendate = scOT.nextLong();

				ticketQueue.add(new Ticket(phonenum, desc, opendate, id));
				if (id > CSS.getCurrentID()){
					CSS.setCurrentID(++id);
				}
			}
		} catch (FileNotFoundException e){
			System.out.println("Could not find Open Tickets file: " + this.openTicketsPath);
		}

		try {
			Scanner scCT = new Scanner(new File(this.closedTicketsPath));

			while (scCT.hasNext()){
				id = scCT.nextLong();
				phonenum = scCT.next();
				lines = scCT.nextInt();
				desc = "";

				scCT.nextLine(); // The scanner is between the int and the newline, so we advance it...

				for (int l = 0; l < lines; l++) {
					desc += scCT.nextLine() + "\n"; // nextLine trims the newline.
				}

				opendate = scCT.nextLong();
				closedate = scCT.nextLong();

				closedTicketList.add(new Ticket(phonenum, desc, opendate, id, closedate));
				if (id > CSS.getCurrentID()){
					CSS.setCurrentID(++id);
				}
			}
		} catch (FileNotFoundException e){
			System.out.println("Could not find Closed Tickets file: " + this.closedTicketsPath);
		}

		return;
	}


}
