//
// File: OM.java
// Author: Daniel Devine
// Purpose: This file is a program for collecting statics from the saved data and output them as text.
//

import java.util.*;
import java.io.*;

public class OM {
	private static LinkedList<Ticket> ticketQueue = new LinkedList<Ticket>(); // Queue for open tickets
	private static LinkedList<Ticket> closedTicketList = new LinkedList<Ticket>(); // Just a list of closed tickets.

	private static String openTicketsPath = "open.txt"; // The path/filename for the open tickets database.
	private static String closedTicketsPath = "closed.txt"; // The path/filename for the closed tickets database.

	private static StorageManager sm; // Reference to our Storage Manager, used for saving and loading the ticket databases.

	// Some constants for converting time units into milliseconds.
	public static final int DAY = 86400000;
	public static final int HOUR = 3600000;
	public static final int MINUTE = 60000;
	public static final int SECOND = 1000;

	public static void main(String[] args){
		try {
			openTicketsPath = args[0];
			closedTicketsPath = args[1];
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Input and/or Output queue file(s) not supplied, trying defaults.");
		}

		boolean open = (new File(openTicketsPath)).exists();
		boolean closed = (new File(closedTicketsPath)).exists();
		if (open != true || closed != true){
			if (open != true && closed != true){
				System.err.println("Open Tickets and Closed Tickets Files not found.");
				System.exit(1);
			} else if(open != true){
				System.err.println("Open Tickets File not found.");
				System.exit(1);
			} else {
				System.err.println("Closed Tickets File not found.");
				System.exit(1);
			}

		}

		sm = new StorageManager(openTicketsPath, closedTicketsPath, ticketQueue, closedTicketList); // We give storage manager a reference to the ticket collections.
		sm.load();

		// Total number of jobs
		System.out.println("Total Tickets: " + (ticketQueue.size() + closedTicketList.size()));

		// Pending jobs
		System.out.println("Total Pending Tickets: " + ticketQueue.size());

		// jobs resolved by CCCs
		int rccc = 0;
		int rtso = 0;
		long longest = 0;
		double average = 0;
		long delta = 0;
		for (Ticket t: closedTicketList){
			if (t.getOpenDate() == t.getCloseDate()){
				rccc++;
			} else {
				rtso++;
				delta = (t.getCloseDate() - t.getOpenDate());
				longest = (longest < delta) ? delta : longest;
				average += delta;
			}
		}

		average = average / (double) closedTicketList.size();

		System.out.println("Tickets Resolved By CCCs: " + rccc);


		// jobs resolved by TSOs
		System.out.println("Tickets Resolved by TSOs: " + rtso);

		// Percentage resolved by CCCs
		System.out.printf("Percentage Resolved by CCCs: %.2f%%\n", (rccc/(double)(rtso + rccc))*(100.0/1));

		// Percentage resolved by TSO
		System.out.printf("Percentage Resolved by TSOs: %.2f%%\n", (rtso/(double)(rtso + rccc))*(100.0/1));

		// Longest time between job being logged and TSO resolving

		double l = longest;
		int lDays = (int) longest / DAY;
		l = longest -(lDays * DAY);

		int lHours = (int) l / HOUR;
		l -= lHours * HOUR;

		int lMinutes = (int) l / MINUTE;
		l -= lMinutes * MINUTE;

		int lSeconds = (int) l / SECOND;

		System.out.printf("Longest Time to Resolution by TSO: %d Days %d Hours %d Minutes %d Seconds\n", lDays, lHours, lMinutes, lSeconds);

		// Average time until resolution by TSO.

		double a = average;
		int aDays = (int) average / DAY;
		a = average -(aDays * DAY);

		int aHours = (int) a / HOUR;
		a -= aHours * HOUR;

		int aMinutes = (int) a / MINUTE;
		a -= aMinutes * MINUTE;

		int aSeconds = (int) a / SECOND;

		System.out.printf("Average Time to Resolution by TSO: %d Days %d Hours %d Minutes %d Seconds\n", aDays, aHours, aMinutes, aSeconds);
	}
	
}
