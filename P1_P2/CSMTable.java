//
// File: CSMTable.java
// Author: Daniel Devine
// Purpose: This defines the data model used by the table in the CSM GUI.
//

import java.awt.event.*;
import java.util.*;
import javax.swing.table.AbstractTableModel;

public class CSMTable extends AbstractTableModel{
	// This class is a Data Model for our CSM GUI JTable.

	private static CSM csm; // A reference to the CSM object.
	private static String[] columnNames = {"ID", "Open Date", "Description..."}; // Headers for the table, needed for getColumnName
	private static Object[][] tableData = {{"","",""}}; // Initialise to an empty 2D array of data to be displayed in the table.

	public CSMTable(CSM csm){
		this.csm = csm;
		if (!csm.isQueueEmpty()){ // If we do not check this, we will get a null pointer error.
			tableData = csm.getTableData();
		}
	}

	public int getColumnCount() {
		// Return the number of columns in the table
		return columnNames.length;
	}

	public int getRowCount() {
		// Return the number of rows in the table, this is the number of open Tickets.
		return tableData.length;
	}

	public String getColumnName(int col) {
		// Retun the name of a column.
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		// Return the value in a table cell.
		return tableData[row][col];
	}

	public boolean isCellEditable(int row, int col) {
		// Returns False, because we do not update data via the table itself.
		return false; // None of the CSM data is editable... Effect is caused by Prioritise button.
	}

	public void setValueAt(Object value, int row, int col) {
		// Set the value in a cell. This shouldn't be used but is required to be defined.
		tableData[row][col] = value;
		fireTableCellUpdated(row, col);
    }

    public void updateAll(){
    	// Update all the cells in a table.
    	if (!csm.isQueueEmpty()){ // If we do not check this, we will get a null pointer error.
			tableData = csm.getTableData();
		} else {
			tableData = new Object[0][0]; // Empty...
		}

		fireTableDataChanged(); // Notifies all cells;
    }
}