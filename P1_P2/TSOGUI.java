//
// File: TSOGUI.java
// Author: Daniel Devine
// Purpose: Defines the Technical Service Officer (TSO) Grapical User Interface. 
//

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.*;

public class TSOGUI {

	private JFrame frame = new JFrame("TSO"); // The Window for the TSO GUI.

	// Components
	private JTextField callerNumberField = new JTextField(15); // The field which shows and allows altering of the caller number.
	private JLabel ticketNumber = new JLabel("(None Yet...)"); // The JLabel which displays the ID of the current ticket.
	private JTextArea descriptionTextArea = new JTextArea(10, 50); // The text area which allows modification of the Ticket description.
	private JLabel dateTimeLabel = new JLabel("Opened: (None Yet...)", JLabel.RIGHT); // The label which shows the open date/time of the ticket.
	private JButton nextTicketBtn = new JButton("Next Ticket"); // The JButton which triggers the retrieval of a new ticket from the queue. (and saves any changes)
	private JButton resolveBtn = new JButton("Resolve"); // The button which saves modifications to the Ticket and puts it in the closed tickets list.

	private TSO tso; // The reference to our TSO.

	private int next = 0; // A number which keeps track of the place in the queue. Used for cycling through the Queue.

	private Ticket currentTicket; // A reference to the current Ticket.


	public TSOGUI(TSO tso){
		this.tso = tso;
		setup_layout();	
		addListeners();
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void setup_layout(){
		// Lay out the interface

		JPanel box0 = new JPanel(); // Wrapper box
		JPanel box1 = new JPanel(); // Holds sub panels...
			JPanel box1a = new JPanel();
			JPanel box1b = new JPanel();
		JPanel box2 = new JPanel(); // Holds Description
		JPanel box3 = new JPanel(); // Holds Escalate/Resolve buttons

		box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS)); // Because we are building layout vertically.
		box1.setLayout(new BoxLayout(box1, BoxLayout.Y_AXIS));
			box1a.setLayout(new BoxLayout(box1a, BoxLayout.X_AXIS));
			box1b.setLayout(new BoxLayout(box1b, BoxLayout.X_AXIS));
		box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS));
		box3.setLayout(new BoxLayout(box3, BoxLayout.X_AXIS));

		frame.add(box0);
		box0.add(box1);
		box0.add(box2);
		box0.add(box3);
		
		box1.add(box1a);
		box1.add(box1b);

		box1.setAlignmentX(Component.CENTER_ALIGNMENT);
			box1a.setAlignmentX(Component.LEFT_ALIGNMENT);
			box1b.setAlignmentX(Component.LEFT_ALIGNMENT);
		box2.setAlignmentX(Component.CENTER_ALIGNMENT);
		box3.setAlignmentX(Component.CENTER_ALIGNMENT);

		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
			box1a.setBorder(new EmptyBorder(5, 5, 5, 5));
			box1b.setBorder(new EmptyBorder(5, 5, 5, 5));
		box2.setBorder(new EmptyBorder(5, 5, 5, 5));
		box3.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Framework done, now for Components.

		JLabel callerNumberLabel = new JLabel("Caller Number");
		callerNumberLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		callerNumberField.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1a.add(callerNumberLabel);
		box1a.add(callerNumberField);

		dateTimeLabel.setBorder(new EmptyBorder(5, 50, 5, 5)); // Note that positions are anti-clockwise unlike CSS.
		box1a.add(dateTimeLabel);

		JLabel ticketNumberLabel = new JLabel("Ticket: ");
		ticketNumberLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		ticketNumber.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1b.add(ticketNumberLabel);
		box1b.add(ticketNumber);
		
		// Add description area...
		descriptionTextArea.setBorder(new EmptyBorder(5, 5, 5, 5));
		descriptionTextArea.setText("Description...");
		box2.add(descriptionTextArea);

		// Add Resolution buttons;
		box3.add(nextTicketBtn);
		box3.add(Box.createRigidArea(new Dimension(50,0))); // Creates separation between buttons.
		box3.add(resolveBtn);

		// Make main frame set size
		frame.setResizable(false);
	}

	private void addListeners(){
		// Add listeners to the interface to respond to user events.

		// Resolve Button
		resolveBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.out.println("RESOLVE BUTTON");
					if (currentTicket != null){
						updateTicket(currentTicket, true);
						tso.closeTicket(currentTicket);
						currentTicket.unlock();
					}
					
					Ticket nextTicket = tso.getNextTicket();
					loadFields(tso.getNextTicket());
					if (currentTicket != null){
						currentTicket.lock();
					}
					next = 0;
				}
			}
		);


		// Escalate Button
		nextTicketBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if (currentTicket != null){
						updateTicket(currentTicket, false);
						currentTicket.unlock();
					}
					Ticket ticket = tso.getNextTicket(next);
					if (ticket != null){
						ticket.lock();
					}
					loadFields(ticket);
					next++;
				}
			}
		);

	} // End addListeners

	private void loadFields(Ticket ticket){
		// Loads the data of the current Ticket into the user interface, or sets defaults if there is no tickets.
		if (ticket != null){
			currentTicket = ticket;
			descriptionTextArea.setText(ticket.getDescription());
			ticketNumber.setText("" + ticket.getID());
			callerNumberField.setText(ticket.getPhoneNumber());
			Date opendate = new Date(ticket.getOpenDate());
			dateTimeLabel.setText("Opened: " + opendate.toString());
		} else {
			currentTicket = null;
			descriptionTextArea.setText("No tickets to display...");
			dateTimeLabel.setText("Opened: (None yet...)");
			ticketNumber.setText("");

		}
	}


	private void updateTicket(Ticket ticket, boolean resolve){
		// Update the Ticket with new data in the interface.
		ticket.setDescription(descriptionTextArea.getText());
		ticket.setPhoneNumber(callerNumberField.getText());
		if (resolve){
			ticket.close();
		}
	}
	
}
