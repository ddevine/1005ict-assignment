//
// File: CSMGUI.java
// Author: Daniel Devine
// Purpose: This defines the graphical interface for the Customer Support Manager (CSM).
//

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.*;
import java.awt.Dimension;

public class CSMGUI {
	// The class which defines the GUI for a CSM.

	private static CSM csm; // A reference to the CSM for this interface.

	private JFrame frame = new JFrame("CSM"); // The Window for this UI.
	private JTable table; // The table in which Tickets will be displayed for the user to select.
	private CSMTable csmtable; // The table Data Model for the table. 
	private JButton refreshBtn = new JButton("Refresh"); // The button to refresh the data being displayed in the table.
	private JButton prioritiseBtn = new JButton("Prioritise"); // The button to call the CSM function to move the Ticket to the head of the queue.
	private JButton saveBtn = new JButton("Save Queues"); // The button to call the CSM method which tells the StorageManager to save the Tickets.

	private static String[] columnNames = {"ID", "Open Date", "Description..."}; // The headers for the table.

	public CSMGUI(CSM csm){
		this.csm = csm;
		setup_layout();	
		addListeners();
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void setup_layout(){
		// Setup the GUI
		JPanel box0 = new JPanel(); // Wrapper container.
		JPanel box1 = new JPanel(); // Container to hold ticket list stuff.
		JPanel box2 = new JPanel(); // Container to hold buttons.

		box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS));
		box1.setLayout(new BoxLayout(box1, BoxLayout.Y_AXIS));
		box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS));

		frame.add(box0);
		box0.add(box1);
		box0.add(box2);

		box0.setAlignmentX(Component.CENTER_ALIGNMENT);
		box1.setAlignmentX(Component.LEFT_ALIGNMENT);
		box2.setAlignmentX(Component.LEFT_ALIGNMENT);

		box0.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
		box2.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Lay out the Ticket table.

		csmtable = new CSMTable(csm);
		table = new JTable(csmtable); 

		table.setPreferredScrollableViewportSize(new Dimension(600, 250));
		table.setFillsViewportHeight(true);
		JScrollPane scroll = new JScrollPane(table);
		box1.add(scroll);

		box1.add(table.getTableHeader(), BorderLayout.PAGE_START);

		box2.add(refreshBtn);
		box2.add(Box.createRigidArea(new Dimension(50,0)));
		box2.add(prioritiseBtn);
		box2.add(Box.createRigidArea(new Dimension(50,0)));
		box2.add(saveBtn);

	}

	private long getSelectedTicketID(){
		// Return the Ticket ID for the selected row. Return -1 if none selected.
		int selection = table.getSelectedRow(); // Ideally this should be a long...
		if (selection > -1){
			int row = table.convertRowIndexToModel(selection);
			return Long.parseLong(table.getValueAt(row, 0).toString());
		} else {
			return -1;
		}

	}

	private void addListeners(){
		// Add the listeners for the interface so that when an event happens methods are called as a result.

		// Refresh Button
		refreshBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.out.println("REFRESH BUTTON");				
					csmtable.updateAll();
					frame.pack();
				}
			}
		);

		// Prioritise Button
		prioritiseBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.out.println("PRIORITISE BUTTON");
					long id = getSelectedTicketID();
					System.out.println("Selected Ticket: " + id);		
					Ticket t = csm.getTicketByID(id);
					csm.prioritise(t);
					csmtable.updateAll();
					frame.pack();
				}
			}
		);

		// Save Button
		saveBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.out.println("SAVE BUTTON");
					csm.save();
					frame.pack();
				}
			}
		);

	} // End addListeners

}
