//
// File: Ticket.java
// Author: Daniel Devine
// Purpose: To define a Ticket object. A ticket contains data for a job.
//

import java.util.Date;
import java.util.*;

public class Ticket {
	// This class defines a Ticket object.
	private String description; // The description of a ticket - ie. "I can't make calls while the vaccum is on."
	private long id = -1; // The ID of the ticket.
	private long openDate; // The date the Ticket was opened, in Unix Time in milliseconds.
	private long closeDate; // The date the Ticket was closed, in Unix Time in milliseconds.
	private String phoneNumber; // The phone number of the caller.
	private boolean locked; // A lock for the ticket so that the ticket is not accidentially worked on by multiple people simultaneously.

	public Ticket(String phoneNumber, String description, long ... optionals){
		// optionals[0] = opendateDate, optionals[1] = id,  optionals[2] = closeDate
		this.openDate = System.currentTimeMillis();
		this.description = description;
		this.phoneNumber = phoneNumber;
		try {
			this.openDate = optionals[0];
			this.id = optionals[1];
			this.closeDate = optionals[2];
		} catch (Exception e) { } // Fail silently.

		if (this.id < 0){
			this.id = CSS.getNewID();
		}

	}

	public long getID(){
		// Return the Ticket ID.
		return this.id;
	}

	public void close(){
		// Close the ticket.
		this.closeDate = System.currentTimeMillis();
		return;
	}

	public void instantClose(){
		// Takes the open time and sets it as closing time - reliably performs a CCC resolution.
		this.closeDate = this.openDate;
	}

	public String getDescription(){
		// Return the Ticket description
		return this.description;
	}

	public long getOpenDate(){
		// Return the ticket Open Date/Time in milliseconds.
		return this.openDate;
	}

	public long getCloseDate(){
		// Returns the ticket Close Date/Time in milliseconds.
		return this.closeDate;
	}

	public String getPhoneNumber(){
		// Returns the Ticket caller number String.
		return this.phoneNumber;
	}

	public int getDescLineCount(){
		// Returns the number of lines in the description. Useful when saving the ticket in a text format.
		Scanner sc = new Scanner(this.description);
		int lines = 0;
		while (sc.hasNextLine()){
			lines++;
			sc.nextLine();
		}
		return lines;
	}

	public void lock(){
		// Locks the ticket
		this.locked = true;
		return;
	}

	public void unlock(){
		// Unlocks the Ticket
		this.locked = false;
		return;
	}

	public boolean isLocked(){
		// Returns true if the ticket is locked for editing.
		return this.locked;
	}

	public void setDescription(String desc){
		// Sets the given string as the Ticket description.
		this.description = desc;
		return;
	}

	public void setPhoneNumber(String num){
		// Sets the given phone number string as the Ticket phone number.
		this.phoneNumber = num;
		return;
	}
}