//
// File: CSS.java
// Author: Daniel Devine
// Purpose: This is a program that lauches the Customer Serivice System as a whole. This initialises all the components and GUIs.
//

import java.util.*;

public class CSS {
	// Class for the Customer Service System

	private static LinkedList<Ticket> ticketQueue = new LinkedList<Ticket>(); // Queue for open tickets
	private static LinkedList<Ticket> closedTicketList = new LinkedList<Ticket>(); // Just a list of closed tickets.

	private static long currentID = 0; // Stores the highest Ticket id

	private static CSM csm; // Reference to our Customer Support Manager

	// Note that multiple CCCs and TSOs are referenced in the situation brief but not needed for the prototype... Design will generally support multiple workers anyway.

	private static String openTicketsPath = "open.txt"; // The path/filename for the open tickets database.
	private static String closedTicketsPath = "closed.txt"; // The path/filename for the closed ticket database.

	private static StorageManager storage; // A reference to our Storage Manager object for interacting with the ticket database.
	
	public static long getNewID(){
		// Increment the current ID and return the result.
		return ++currentID;
	}

	public static long getCurrentID(){
		// Return the current highest Ticket ID
		return currentID;
	}

	public static void setCurrentID(long id){
		// Set the highest Ticket ID. Tickets created after this will be higher than this number.
		currentID = id;
	}

	public static void main(String[] args){
		try {
			openTicketsPath = args[0];
			closedTicketsPath = args[1];
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Input and Output queue files not supplied, defaulting.");
		}

		storage = new StorageManager(openTicketsPath, closedTicketsPath, ticketQueue, closedTicketList); // We give storage manager a reference to the ticket collections.
		storage.load();

		CCC ccc = new CCC(ticketQueue, closedTicketList);
		CCCGUI cccGUI = new CCCGUI(ccc); // Attach a GUI to our CCC.
		
		TSO tso = new TSO(ticketQueue, closedTicketList);
		TSOGUI tsoGUI = new TSOGUI(tso);

		CSM csm = new CSM(ticketQueue, closedTicketList, storage);
		CSMGUI csmGUI = new CSMGUI(csm);
	}
}
