//
// File: CCC.java
// Author: Daniel Devine
// Purpose: This defines a Call Center Consultant (CCC) object, which performs the actions of a CCC.
//

import java.util.*;

public class CCC {
	// The Call Center Consultant class

	private static LinkedList<Ticket> ticketQueue; // The queue for open tickets.
	private static LinkedList<Ticket> closedTicketList; // The list of closed tickets.

	public CCC(LinkedList<Ticket> ticketQueue, LinkedList<Ticket> closedTicketList){
		this.ticketQueue = ticketQueue;
		this.closedTicketList = closedTicketList;
	}

	public Ticket newTicket(String phoneNumber, String description){
		// Creates and returns a Ticket.
		return new Ticket(phoneNumber, description);
	}

	public void advanceTicket(Ticket ticket){
		// Escalate ticket (add to) open tickets queue (as opposed to closing and adding to closed tickets list)
		ticketQueue.add(ticket);
	}

	public void closeTicket(Ticket ticket){
		// Close ticket and store in closed ticket list.
		ticket.instantClose();
		closedTicketList.add(ticket);
	}

}