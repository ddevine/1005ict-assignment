//
// File: TSO.java
// Author: Daniel Devine
// Purpose: Defines a Technical Service Officer (TSO) object, which peforms the actions of a TSO.
//

import java.util.*;

public class TSO {
	// The Technical Service Officer class.

	private static LinkedList<Ticket> ticketQueue; // Queue for open tickets
	private static LinkedList<Ticket> closedTicketList; // List for closed tickets.

	public TSO(LinkedList<Ticket> ticketQueue, LinkedList<Ticket> closedTicketList) {
		this.ticketQueue = ticketQueue;
		this.closedTicketList = closedTicketList;
	}

	public Ticket getNextTicket(){
		// Get a Ticket from the advanced ticket queue.
		// Return null if no tickets availabe.
		Ticket t = getNextTicket(0);
		if (t != null){
			t.lock();
		}
		return t;
	}

	public Ticket getNextTicket(int n){
		// Return the nth next available ticket, return null if none available.
		if (ticketQueue.size() > 0){
			n =  n % ticketQueue.size();
			int found = 0;
			int tries = 0;
			while ((found < n+1) && tries < ticketQueue.size()){
				if (!ticketQueue.get(tries).isLocked()){
					tries++;
					found++;
				} else {
					tries++;
				}
			}
			return ticketQueue.get(tries - 1);
		} else {
			return null;
		}
	}

	public void closeTicket(Ticket ticket){
		// Moves a ticket from the ticketQueue to closedTicketList
		ticketQueue.remove(ticket);
		closedTicketList.add(ticket);
		return;
	}
	
}