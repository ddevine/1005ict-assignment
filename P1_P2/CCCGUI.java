//
// File: CCCGUI.java
// Author: Daniel Devine
// Purpose: This defines the Call Center Consultant (CCC) graphical interface.
//

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.*;

public class CCCGUI {
	// The class for the Call Center Consultant GUI.

	private JFrame frame = new JFrame("CCC"); // The Window for the Call Center Consultant UI.

	// Components
	private JTextField callerNumberField = new JTextField(15); // The field the phone number is entered in.
	private JLabel ticketNumber = new JLabel("(None Yet...)"); // The previous ticket number (so that the consultant can tell the caller what the ticket was lodged as)
	private JTextArea descriptionTextArea = new JTextArea(10, 50); // The area for the ticket description to be entered.
	private JLabel dateTimeLabel = new JLabel(new Date().toString(), JLabel.RIGHT); // The label where the current date/time will be displayed.
	private JButton escalateBtn = new JButton("Escalate"); // The button to add the ticket to the queue for solving by a TSO.
	private JButton resolveBtn = new JButton("Resolve"); // The button for closing a ticket and adding it to the closed ticket list.

	private CCC ccc; // The reference to our CCC object, which we call to do all the Ticket/queue tasks.


	public CCCGUI(CCC ccc){
		this.ccc = ccc;
		setup_layout();	
		addListeners();
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	private void setup_layout(){
		// Set up the layout of the interface. 

		JPanel box0 = new JPanel(); // Wrapper box
		JPanel box1 = new JPanel(); // Holds sub panels...
			JPanel box1a = new JPanel();
			JPanel box1b = new JPanel();
		JPanel box2 = new JPanel(); // Holds Description
		JPanel box3 = new JPanel(); // Holds Escalate/Resolve buttons

		box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS)); // Because we are building layout vertically.
		box1.setLayout(new BoxLayout(box1, BoxLayout.Y_AXIS));
			box1a.setLayout(new BoxLayout(box1a, BoxLayout.X_AXIS));
			box1b.setLayout(new BoxLayout(box1b, BoxLayout.X_AXIS));
		box2.setLayout(new BoxLayout(box2, BoxLayout.X_AXIS));
		box3.setLayout(new BoxLayout(box3, BoxLayout.X_AXIS));

		frame.add(box0);
		box0.add(box1);
		box0.add(box2);
		box0.add(box3);

		box1.add(box1a);
		box1.add(box1b);

		box1.setAlignmentX(Component.CENTER_ALIGNMENT);
			box1a.setAlignmentX(Component.LEFT_ALIGNMENT);
			box1b.setAlignmentX(Component.LEFT_ALIGNMENT);
		box2.setAlignmentX(Component.CENTER_ALIGNMENT);
		box3.setAlignmentX(Component.CENTER_ALIGNMENT);

		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
			box1a.setBorder(new EmptyBorder(5, 5, 5, 5));
			box1b.setBorder(new EmptyBorder(5, 5, 5, 5));
		box2.setBorder(new EmptyBorder(5, 5, 5, 5));
		box3.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Framework done, now for Components.

		JLabel callerNumberLabel = new JLabel("Caller Number");
		callerNumberLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		callerNumberField.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1a.add(callerNumberLabel);
		box1a.add(callerNumberField);

		dateTimeLabel.setBorder(new EmptyBorder(5, 50, 5, 5)); // Note that positions are anti-clockwise unlike CSS.
		box1a.add(dateTimeLabel);

		JLabel ticketNumberLabel = new JLabel("Previous Ticket: ");
		ticketNumberLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
		ticketNumber.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1b.add(ticketNumberLabel);
		box1b.add(ticketNumber);
		
		// Add description area...
		descriptionTextArea.setBorder(new EmptyBorder(5, 5, 5, 5));
		descriptionTextArea.setText("Description...");
		box2.add(descriptionTextArea);

		// Add Resolution buttons;
		box3.add(escalateBtn);
		box3.add(Box.createRigidArea(new Dimension(50,0))); // Creates separation between buttons.
		box3.add(resolveBtn);

		// Make main frame set size
		frame.setResizable(false);
	}

	private void addListeners(){
		// Add the listeners for the interface so that when an event happens methods are called as a result.

		// Resolve Button
		resolveBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e){
					System.out.println("RESOLVE BUTTON");
					if (callerNumberField.getText().compareToIgnoreCase("") != 0){ // We don't want empty tickets.
						Ticket t = ccc.newTicket(callerNumberField.getText(), descriptionTextArea.getText());
						ccc.closeTicket(t);
						ticketNumber.setText("" + t.getID());
						resetFields();
					} else {
						JOptionPane.showMessageDialog(frame, "You must enter a Caller Number");
					}
				}
			}
		);


		// Escalate Button
		escalateBtn.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					System.out.println("ESCALATE BUTTON");
					if (callerNumberField.getText().compareToIgnoreCase("") != 0){
						Ticket t = ccc.newTicket(callerNumberField.getText(), descriptionTextArea.getText());
						ccc.advanceTicket(t);
						ticketNumber.setText("" + t.getID());
						resetFields();
					} else {
						JOptionPane.showMessageDialog(frame, "You must enter a Caller Number");
					}
				}
			}
		);

	} // End addListeners

	private void resetFields(){
		// Reset all the entry fields ready for the next ticket to be added.
		descriptionTextArea.setText("Description...");
		callerNumberField.setText("");
		dateTimeLabel.setText(new Date().toString());
	}
	
}
