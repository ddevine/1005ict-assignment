//
// File: CSM.java
// Author: Daniel Devine
// Purpose: This defines a Customer Service Manager (CSM) object, which performs the actions of a CSM.
//

import java.util.*;

public class CSM {
	// The Customer Service Manager class

	private static LinkedList<Ticket> ticketQueue; // A reference to the open tickets queue
	private static LinkedList<Ticket> closedTicketList; // A reference to the close ticket list
	private static StorageManager storage; // A reference to the Storage Manager for saving the tickets.

	public CSM(LinkedList<Ticket> ticketQueue, LinkedList<Ticket> closedTicketList, StorageManager storage){
		this.ticketQueue = ticketQueue;
		this.closedTicketList = closedTicketList;
		this.storage = storage;
	}

	public boolean isQueueEmpty(){
		// Returns true if the open tickets queue is empty.
		return (ticketQueue == null || ticketQueue.size() == 0) ? true : false; // Fully sick ternary statement.
	}

	public void save(){
		// Tells the Storage Manager to save all Tickets.
		storage.save();
	}

	public Object[][] getTableData(){
		// Returns a 2D array of Ticket data for display in the overview JTable.
		Object[][] data = new Object[ticketQueue.size()][3];
		for (int i = 0; i < ticketQueue.size(); i++){
			Ticket t = ticketQueue.get(i);
			data[i][0] = t.getID();
			data[i][1] = new Date(t.getOpenDate());
			data[i][2] = t.getDescription();
		}
		return data;
	}

	public Ticket getTicketByID(long id){
		// Returns a Ticket object from the open tickets queue given an ID.
		for (Ticket t: ticketQueue){
			if (t.getID() == id){
				return t;
			}
		}
		return null;
	}

	public void prioritise(Ticket t){
		// Puts a Ticket at the head of the open tickets queue.
		ticketQueue.remove(t);
		ticketQueue.addFirst(t);
	}

}